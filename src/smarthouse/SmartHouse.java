package smarthouse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import smarthouse.music.KenwoodRadio;
import smarthouse.music.MusicPlayer;
import smarthouse.music.PioneerRadio;
import smarthouse.printers.LGPrinter;
import smarthouse.printers.Printer;
import smarthouse.tv.LGTV;
import smarthouse.tv.PanasonicTV;
import smarthouse.tv.SamsungTV;
import smarthouse.tv.TV;

public class SmartHouse {
    private List<MusicPlayer> musicPlayerList = new ArrayList();
    private Printer printer;
    private List<TV> tvList;
    private List<Devices> devicesList;

    public SmartHouse() {
        this.musicPlayerList.add(new KenwoodRadio(Rooms.LIVINGROOM));
        this.musicPlayerList.add(new KenwoodRadio(Rooms.BATHROOM));
        this.musicPlayerList.add(new PioneerRadio(Rooms.BEDROOM));
        this.printer = new LGPrinter(Rooms.LIVINGROOM);
        this.tvList = new ArrayList();
        this.tvList.add(new LGTV(Rooms.BEDROOM));
        this.tvList.add(new PanasonicTV(Rooms.LIVINGROOM));
        this.tvList.add(new SamsungTV(Rooms.BATHROOM));
        this.devicesList = new ArrayList();
        this.devicesList.addAll(this.musicPlayerList);
        this.devicesList.addAll(this.tvList);
        this.devicesList.add(this.printer);
    }

    public void turnOffAllDevices() {
        Iterator var1 = this.devicesList.iterator();

        while(var1.hasNext()) {
            Devices device = (Devices)var1.next();
            device.turnOff();
        }

    }

    public void printerOn() {
        this.printer.turnOn();
    }

    public void printerOff() {
        this.printer.turnOff();
    }

    public void print(String text) {
        this.printerOn();
        this.printer.print(text);
    }

    public void printBlackWhite(String text) {
        this.printer.printInBlackWhite(text);
    }

    public void turnOnTv(Rooms room) {
        Iterator var2 = this.tvList.iterator();

        while(var2.hasNext()) {
            TV tv = (TV)var2.next();
            if (tv.getRoom() == room) {
                tv.turnOn();
            }
        }

    }

    public void turnOffTV(Rooms room) {
        Iterator var2 = this.tvList.iterator();

        while(var2.hasNext()) {
            TV tv = (TV)var2.next();
            if (tv.getRoom() == room) {
                tv.turnOff();
            }
        }

    }

    public void turnOffAllTV() {
        Iterator var1 = this.tvList.iterator();

        while(var1.hasNext()) {
            TV tv = (TV)var1.next();
            tv.turnOff();
        }

    }

    public void turnOnRadio(Rooms room) {
        Iterator var2 = this.musicPlayerList.iterator();

        while(var2.hasNext()) {
            MusicPlayer musicPlayer = (MusicPlayer)var2.next();
            if (musicPlayer.getRoom() == room) {
                musicPlayer.turnOn();
            }
        }

    }

    public void turnOffRadio(Rooms room) {
        Iterator var2 = this.musicPlayerList.iterator();

        while(var2.hasNext()) {
            MusicPlayer musicPlayer = (MusicPlayer)var2.next();
            if (musicPlayer.getRoom() == room) {
                musicPlayer.turnOn();
            }
        }

    }
}
