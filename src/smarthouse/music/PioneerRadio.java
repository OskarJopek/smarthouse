package smarthouse.music;

import smarthouse.Rooms;

public class PioneerRadio extends MusicPlayer {
    private Rooms room;

    public PioneerRadio(Rooms room) {
        super(room);
    }

    public void turnOff() {
        System.out.println("music device turned off");
    }

    public void turnOn() {
        System.out.println("music device turned on");
    }
}
