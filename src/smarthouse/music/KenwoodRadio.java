package smarthouse.music;

import smarthouse.Rooms;

public class KenwoodRadio extends MusicPlayer {

    public KenwoodRadio(Rooms room) {
        super(room);
    }

    public void turnOff() {
        System.out.println("music device turned off");
    }

    public void turnOn() {
        System.out.println("music device turned on");
    }
}