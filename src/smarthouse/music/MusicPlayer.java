package smarthouse.music;

import smarthouse.Devices;
import smarthouse.Rooms;

public abstract class MusicPlayer implements Devices {
    private Rooms room;

    public MusicPlayer(Rooms room) {
        this.room = room;
    }

    public abstract void turnOff();

    public abstract void turnOn();

    public Rooms getRoom() {
        return this.room;
    }
}
