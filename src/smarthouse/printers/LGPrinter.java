package smarthouse.printers;

import smarthouse.Rooms;
import smarthouse.printers.drivers.LGPrinterDriver;

public class LGPrinter implements Printer {
    LGPrinterDriver driver;
    private Rooms room;

    public LGPrinter(Rooms room) {
        this.room = room;
        this.driver = new LGPrinterDriver();
    }

    public void print(String text) {
        this.driver.printNow(text);
    }

    public void printInBlackWhite(String text) {
        this.driver.printNowInBlackWhite(text);
    }

    public void turnOn() {
        this.driver.switchOn();
    }

    public void turnOff() {
        this.driver.switchOff();
    }
}
