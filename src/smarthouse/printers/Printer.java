package smarthouse.printers;

import smarthouse.Devices;

public interface Printer extends Devices {
    void print(String var1);

    void printInBlackWhite(String var1);
}