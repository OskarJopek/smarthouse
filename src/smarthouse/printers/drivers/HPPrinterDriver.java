package smarthouse.printers.drivers;

public class HPPrinterDriver {
    public HPPrinterDriver() {
    }

    public void ptint(String text) {
        System.out.println("HP printer: " + text);
    }

    public void printerOn() {
        System.out.println("HP printer is working");
    }

    public void printerOff() {
        System.out.println("HP printer shut down");
    }
}