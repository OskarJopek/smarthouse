package smarthouse.printers;

import smarthouse.Rooms;
import smarthouse.printers.drivers.HPPrinterDriver;

public class HPPrinter implements Printer {
    HPPrinterDriver driver;
    private Rooms room;

    public HPPrinter(Rooms room) {
        this.room = room;
        this.driver = new HPPrinterDriver();
    }

    public void print(String text) {
        this.driver.ptint(text);
    }

    public void printInBlackWhite(String text) {
        System.out.println("HP printer does not print in black-white mode");
    }

    public void turnOn() {
        this.driver.printerOn();
    }

    public void turnOff() {
        this.driver.printerOff();
    }
}