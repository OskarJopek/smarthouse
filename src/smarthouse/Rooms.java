package smarthouse;

public enum Rooms {
    BATHROOM,
    LIVINGROOM,
    BEDROOM;

    private Rooms() {
    }
}