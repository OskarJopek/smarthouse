package smarthouse.tv;

import smarthouse.Rooms;
import smarthouse.tv.drivers.SonyTvDriver;

public class SonyTV implements TV {
    SonyTvDriver driver = new SonyTvDriver();
    Rooms room;

    public SonyTV(Rooms room) {
        this.room = room;
    }

    public void turnOff() {
        this.driver.sonyOff();
    }

    public Rooms getRoom() {
        return this.room;
    }

    public void turnOn() {
        this.driver.sonyOn();
    }
}