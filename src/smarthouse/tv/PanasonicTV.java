package smarthouse.tv;

import smarthouse.Rooms;
import smarthouse.tv.drivers.PanasonicTvDriver;

public class PanasonicTV implements TV {
    PanasonicTvDriver driver = new PanasonicTvDriver();
    Rooms room;

    public PanasonicTV(Rooms room) {
        this.room = room;
    }

    public void turnOff() {
        this.driver.Off();
    }

    public Rooms getRoom() {
        return this.room;
    }

    public void turnOn() {
        this.driver.On();
    }
}