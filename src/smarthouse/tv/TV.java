package smarthouse.tv;

import smarthouse.Devices;
import smarthouse.Rooms;

public interface TV extends Devices {
    Rooms getRoom();
}