package smarthouse.tv;

import smarthouse.Rooms;
import smarthouse.tv.drivers.LGTvDriver;

public class LGTV implements TV {
    LGTvDriver driver = new LGTvDriver();
    Rooms room;

    public LGTV(Rooms room) {
        this.room = room;
    }

    public void turnOff() {
        this.driver.turnOff();
    }

    public Rooms getRoom() {
        return this.room;
    }

    public void turnOn() {
        this.driver.turnOn();
    }
}
