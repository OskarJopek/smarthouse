package smarthouse.tv;

import smarthouse.Rooms;
import smarthouse.tv.drivers.SamsungTvDriver;

public class SamsungTV implements TV {
    SamsungTvDriver driver = new SamsungTvDriver();
    Rooms room;

    public SamsungTV(Rooms room) {
        this.room = room;
    }

    public void turnOff() {
        this.driver.switchOff();
    }

    public Rooms getRoom() {
        return this.room;
    }

    public void turnOn() {
        this.driver.switchOn();
    }
}