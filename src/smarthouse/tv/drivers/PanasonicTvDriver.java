package smarthouse.tv.drivers;

public class PanasonicTvDriver {
    public PanasonicTvDriver() {
    }

    public void On() {
        System.out.println("Panasonic tv turned on");
    }

    public void Off() {
        System.out.println("Panasonic tv turned off");
    }
}
